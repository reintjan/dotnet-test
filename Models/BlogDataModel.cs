namespace dotnet_test.Models
{
  using System;

  public class BlogDataModel{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public int Content { get; set; }
  }
}