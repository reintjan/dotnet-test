﻿namespace dotnet_test.Controllers
{
  using System.Collections.Generic;
  using System.Linq;
  using Microsoft.AspNetCore.Mvc;
  using Services;
  using Models;

  [Route("api/[controller]")]
  public class ValuesController : Controller
  {
    private readonly IBlogService _blogService;
    public ValuesController(IBlogService blogService)
    {
      _blogService = blogService;
    }

    // GET api/values
    [HttpGet]
    public IEnumerable<BlogDataModel> Get()
    {
      var blogItems = _blogService.Get();

      
      return blogItems.ToList().Where(x=> x.Title.StartsWith("Rj"));;
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
    }

    // PUT api/values/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
}
