namespace dotnet_test.Services
{
  using System.Collections.Generic;
  using Models;
  
  public interface IBlogService{
    IEnumerable<BlogDataModel> Get();
  }
}